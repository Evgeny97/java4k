package ru.nsu.fit.belov.crudRepositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.belov.entities.NodeORM;
import ru.nsu.fit.belov.entities.RelationORM;

@Transactional
public interface RelationRepository extends CrudRepository<RelationORM, Integer> {
}
