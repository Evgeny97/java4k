package ru.nsu.fit.belov.crudRepositories;

import org.openstreetmap.osm._0.Way;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.belov.entities.NodeORM;
import ru.nsu.fit.belov.entities.WayORM;

@Transactional
public interface WayRepository extends CrudRepository<WayORM, Integer> {
}
