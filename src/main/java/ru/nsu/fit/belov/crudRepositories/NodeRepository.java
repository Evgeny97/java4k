package ru.nsu.fit.belov.crudRepositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.belov.entities.NodeORM;

@Transactional
public interface NodeRepository extends CrudRepository<NodeORM, Long> {
}
