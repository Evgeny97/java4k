package ru.nsu.fit.belov;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Relation;
import org.openstreetmap.osm._0.Tag;
import org.openstreetmap.osm._0.Way;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.nsu.fit.belov.crudRepositories.NodeRepository;
import ru.nsu.fit.belov.crudRepositories.RelationRepository;
import ru.nsu.fit.belov.crudRepositories.WayRepository;
import ru.nsu.fit.belov.entities.NodeORM;
import ru.nsu.fit.belov.entities.RelationORM;
import ru.nsu.fit.belov.entities.WayORM;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);
    //TODO create EXTENSION postgis
    //    @Autowired
//    ApplicationContext context;
    @Autowired
    NodeRepository nodeRepository;
    @Autowired
    RelationRepository relationRepository;
    @Autowired
    WayRepository wayRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {
            if (false) {
                GeometryFactory factory = new GeometryFactory();
                try (StaxStreamProcessor processor = new StaxStreamProcessor(addNamespaceToStream(new FileInputStream("RU-NVS.osm")))) {
                    XMLEventReader reader = processor.getReader();
                    int nodeCounter = 0;
                    int relationCounter = 0;
                    int wayCounter = 0;
                    ArrayList<NodeORM> nodeList = new ArrayList<>(1000);
                    ArrayList<RelationORM> relationList = new ArrayList<>(1000);
                    ArrayList<WayORM> wayList = new ArrayList<>(1000);
                    while (reader.hasNext()) {       // while not end of XML

                        XMLEvent xmlEvent = reader.peek();   // read next event
                        if (xmlEvent.isStartElement()) {
                            StartElement startElement = xmlEvent.asStartElement();
                            if (startElement.getName().getLocalPart().equals("node")) {
                                nodeCounter++;
                                Node node = JaxbWorker.getNode(reader);
                                NodeORM nodeORM = NodeORM.builder()
                                        .nodeId(node.getId().longValue())
                                        .location(factory.createPoint(new Coordinate(node.getLat(), node.getLon())))
                                        .uid(node.getUid().longValue())
                                        .user(node.getUser())
                                        .visible(node.isVisible())
                                        .version(node.getVersion().longValue())
                                        .changeset(node.getChangeset().longValue())
                                        .timestamp(new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()))
                                        .tag(tagsToMap(node.getTag()))
                                        .build();
                                nodeList.add(nodeORM);
                                if (nodeCounter % 1000 == 0) {
                                    writeNodeBatch(nodeCounter, nodeList);
                                }
                            }
                            if (startElement.getName().getLocalPart().equals("relation")) {
                                relationCounter++;
                                Relation relation = JaxbWorker.getRelation(reader);
                                RelationORM relationORM = RelationORM.builder()
                                        .relationId(relation.getId().longValue())
                                        .user(relation.getUser())
                                        .uid(relation.getUid().longValue())
                                        .visible(relation.isVisible())
                                        .version(relation.getVersion().longValue())
                                        .changeset(relation.getChangeset().longValue())
                                        .timestamp(new Timestamp(relation.getTimestamp().toGregorianCalendar().getTimeInMillis()))
                                        .tag(tagsToMap(relation.getTag()))
                                        .build();
                                relationList.add(relationORM);
                                if (relationCounter % 1000 == 0) {
                                    writeRelationBatch(relationCounter, relationList);
                                }
                            }
                            if (startElement.getName().getLocalPart().equals("way")) {
                                wayCounter++;
                                Way way = JaxbWorker.getWay(reader);
                                WayORM wayORM = WayORM.builder()
                                        .wayId(way.getId().longValue())
                                        .uid(way.getUid().longValue())
                                        .user(way.getUser())
                                        .visible(way.isVisible())
                                        .version(way.getVersion().longValue())
                                        .changeset(way.getChangeset().longValue())
                                        .timestamp(new Timestamp(way.getTimestamp().toGregorianCalendar().getTimeInMillis()))
                                        .tag(tagsToMap(way.getTag()))
                                        .build();
                                wayList.add(wayORM);
                                if (wayCounter % 1000 == 0) {
                                    writeWayBatch(wayCounter, wayList);
                                }
                            }
                        }
                        reader.nextEvent();
                    }
                    writeNodeBatch(nodeCounter, nodeList);
                    writeRelationBatch(relationCounter, relationList);
                    writeWayBatch(wayCounter, wayList);
                }
            }
        };
    }

    private void writeWayBatch(int wayCounter, ArrayList<WayORM> wayList) {
        System.out.println("Way number = " + wayCounter);
        wayRepository.saveAll(wayList);
        wayList.clear();
    }

    private void writeRelationBatch(int relationCounter, ArrayList<RelationORM> relationList) {
        System.out.println("Relation number = " + relationCounter);
        relationRepository.saveAll(relationList);
        relationList.clear();
    }

    private void writeNodeBatch(int nodeCounter, ArrayList<NodeORM> nodeList) {
        if (nodeCounter % 50_000 == 0) {
            System.out.println("Node number = " + nodeCounter);
        }
        nodeRepository.saveAll(nodeList);
        nodeList.clear();
    }

    private Map<String, String> tagsToMap(List<Tag> tags) {
        Map<String, String> map = new HashMap<>();
        for (Tag tag : tags) {
            map.put(tag.getK(), tag.getV());
        }
        return map;
    }

    private InputStream addNamespaceToStream(InputStream inputStream) {
        try {
            String str = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                    "<osm xmlns=\"http://openstreetmap.org/osm/0.6\" version=\"0.6\" generator=\"Osmosis 0.41-44-g988e1a9\">\n";
            InputStream nsStream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
            inputStream.skip(96);
            inputStream = new SequenceInputStream(nsStream, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inputStream;
    }
}

