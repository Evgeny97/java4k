package ru.nsu.fit.belov;

import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Relation;
import org.openstreetmap.osm._0.Way;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamReader;
import java.io.File;

public class JaxbWorker {
    private static JAXBContext jaxbContext;
    private static Unmarshaller un;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(Node.class,Way.class,Relation.class);
            un = jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public JaxbWorker() throws JAXBException {
    }

    public static Node getNode(XMLEventReader streamReader) {
        try {
            return un.unmarshal(streamReader,Node.class).getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Way getWay(XMLEventReader streamReader) {
        try {
            return un.unmarshal(streamReader, Way.class).getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Relation getRelation(XMLEventReader streamReader) {
        try {
            return un.unmarshal(streamReader,Relation.class).getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

}