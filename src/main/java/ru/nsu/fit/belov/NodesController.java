package ru.nsu.fit.belov;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.belov.crudRepositories.NodeRepository;
import ru.nsu.fit.belov.entities.NodeORM;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.Optional;

@RestController
public class NodesController {

    @Autowired
    EntityManager entityManager;
    //http://localhost:8080/near?lat=55.1&lon=82&radius=80

    @GetMapping("/near")
    public List<NodeORM> showStatus(@RequestParam(value="lat", required=false) Double lat,
                                    @RequestParam(value="lon", required=false) Double lon,
                                    @RequestParam(value="radius", required=false) Double radius) {
        if (lat == null){
            lat = 55.;
        }
        if (lon == null){
            lon = 82.;
        }
        if (radius == null){
            radius = 100.;
        }
        Query query = entityManager.createNativeQuery("SELECT * FROM NodeORM WHERE ST_DWithin(location, cast(ST_MakePoint(?,?) as geography), ?)",NodeORM.class);
        query.setParameter(1, lat);
        query.setParameter(2, lon);
        query.setParameter(3, radius);

        List<NodeORM> resultList = query.getResultList();
        return resultList;

    }
}
