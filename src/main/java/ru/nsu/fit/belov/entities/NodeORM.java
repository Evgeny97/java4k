package ru.nsu.fit.belov.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import com.vividsolutions.jts.geom.Point;
import ru.nsu.fit.belov.JsonToPointDeserializer;
import ru.nsu.fit.belov.PointToJsonSerializer;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(indexes = {@Index(columnList = "nodeId", unique = true)})
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
@Builder
public class NodeORM {
    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    @Getter
    @Setter
    protected Map<String, String> tag;

//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @GeneratedValue(generator = "optimized-sequence")
    @Getter
    @Setter
    protected long id;
    @Getter
    @Setter
    protected long nodeId;
    @Getter
    @Setter
    @JsonSerialize(using = PointToJsonSerializer.class)
    @JsonDeserialize(using = JsonToPointDeserializer.class)
    protected Point location;
    @Getter
    @Setter
    @Column(name = "user_name")
    protected String user;
    @Getter
    @Setter
    protected long uid;
    @Getter
    @Setter
    protected Boolean visible;
    @Getter
    @Setter
    protected long version;
    @Getter
    @Setter
    protected long changeset;
    @Getter
    @Setter
    protected Timestamp timestamp;


}
