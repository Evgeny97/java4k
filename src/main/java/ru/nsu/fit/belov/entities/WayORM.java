package ru.nsu.fit.belov.entities;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.openstreetmap.osm._0.Nd;
import org.openstreetmap.osm._0.Tag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
@Builder
public class WayORM {
//    protected List<Nd> nd;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    @Getter
    @Setter
    protected Map<String, String> tag;
    @Id
    @GeneratedValue(generator = "optimized-sequence")
    @Getter
    @Setter
    protected long id;
    @Getter
    @Setter
    protected long wayId;
    @Getter
    @Setter
    @Column(name = "user_name")
    protected String user;
    @Getter
    @Setter
    protected long uid;
    @Getter
    @Setter
    protected Boolean visible;
    @Getter
    @Setter
    protected long version;
    @Getter
    @Setter
    protected long changeset;
    @Getter
    @Setter
    protected Timestamp timestamp;
}
