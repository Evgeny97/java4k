package ru.nsu.fit.belov;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.InputStream;

public class StaxStreamProcessor implements AutoCloseable {
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();


    private final XMLEventReader reader;

    public  StaxStreamProcessor(InputStream is) throws XMLStreamException {
//        XMLStreamReader xmlStreamReader = new XsiTypeReader(FACTORY.createXMLStreamReader(is));
        reader  = FACTORY.createXMLEventReader(is);
    }

    public XMLEventReader  getReader() {
        return reader;
    }

    @Override
    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException e) { // empty
            }
        }
    }
}

/*class XsiTypeReader extends EventR {

    public XsiTypeReader(XMLStreamReader reader) {
        super(reader);
    }

    @Override
    public String getAttributeNamespace(int arg0) {
        if("type".equals(getAttributeLocalName(arg0))) {
            return "http://www.w3.org/2001/XMLSchema-instance";
        }
        return super.getAttributeNamespace(arg0);
    }

    @Override
    public String getNamespaceURI(int index) {
        if("type".equals(getAttributeLocalName(index))) {
            return "http://www.w3.org/2001/XMLSchema-instance";
        }
        return super.getNamespaceURI(index);
    }
}*/